import React from "react";
import { JiraTable } from "./components";

function App() {
  return (
    <div className="App">
      <JiraTable />
    </div>
  );
}

export default App;
