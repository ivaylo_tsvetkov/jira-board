import React from "react";
import { render, cleanup } from "@testing-library/react";
import { JiraTable } from ".";
import { store } from "../../redux/store";
import { Provider } from "react-redux";
import { ModalProps } from "@mui/material";

jest.mock("@mui/material", () => {
    const originalModule = jest.requireActual("@mui/material");
    return {
      __esModule: true,
      ...originalModule,
      Modal: ({children}: ModalProps) => children,
    };
  });

afterEach(cleanup);

const a = { cards: [] };

describe("test JiraTable", () => {
  it("test1", () => {
    const { asFragment } = render(
      <Provider store={store}>
        <JiraTable />
      </Provider>
    );
    expect(asFragment()).toMatchSnapshot();
  });
});
