import React from "react";
import { Grid } from "@mui/material";
import { useSelector } from "react-redux";
import { cardsSelector } from "../../redux/redux-slices/card-slice/selectors";
import labels from "../../labels";

import { JiraColumn } from "../../components";

export const JiraTable = () => {
  const allCards = useSelector(cardsSelector);
  const filter = (f: string) => {
    return allCards.filter((c) => c?.label === f);
  };

  return (
    <Grid container spacing={1} wrap="nowrap">
      {Object.values(labels).map(({ value, text }) => {
        return (
          <JiraColumn
            key={value}
            title={text}
            label={value}
            cards={filter(value)}
          />
        );
      })}
    </Grid>
  );
};
