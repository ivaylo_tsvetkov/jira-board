export const validation = (data: {label:string, points: number, title: string, content: string, assignee: string}) => {
    for (const [key, value] of Object.entries(data)) {
        if (!value) {
            return `Field ${key} is require`
        }
    }
    return false
};
