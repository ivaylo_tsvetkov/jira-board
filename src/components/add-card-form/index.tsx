import React, { useState } from "react";
import {
  Button,
  Grid,
  TextField,
  Select,
  MenuItem,
  Alert,
} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { createdCard } from "../../redux/redux-slices/card-slice";
import labels from "../../labels";
import { cardsSelector } from "../../redux/redux-slices/card-slice/selectors";
import { validation as validationFunc } from "./validation";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
  ".input": {
    width: 300,
    margin: 1,
  },
};

export const AddCardForm = ({
  defaultLabel,
  handleClose,
}: {
  defaultLabel: string;
  handleClose: () => void;
}) => {
  const [label, setLabel] = useState(defaultLabel);
  const [points, setPoints] = useState<number>(0);
  const [assignee, setAssignee] = useState("");
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const dispatch = useDispatch();
  const cards = useSelector(cardsSelector);
  const [validationChek, setValidationChek] = useState<string | boolean>(false);
  const handleClick = () => {
    const validation = validationFunc({
      label,
      points,
      assignee,
      title,
      content,
    });
    if (validation) {
      setValidationChek(validation);
    } else {
      handleClose();
      dispatch(
        createdCard({
          id: cards.length,
          label,
          points,
          assignee,
          title,
          content,
        })
      );
    }
  };
  return (
    <Grid container sx={style}>
      <TextField
        size="small"
        className="input"
        placeholder="Title"
        value={title}
        onChange={(e) => setTitle(e.target.value)}
      />
      <Grid item xs={12}>
        <TextField
          size="small"
          multiline
          minRows={3}
          maxRows={3}
          className="input"
          placeholder="Content"
          value={content}
          onChange={(e) => setContent(e.target.value)}
        />
      </Grid>
      <Select
        className="input"
        size="small"
        variant="outlined"
        placeholder="Label"
        value={label}
        onChange={(e) => setLabel(e.target.value)}
      >
        {Object.values(labels).map(({ value, text }) => {
          return (
            <MenuItem key={value} value={value}>
              {text}
            </MenuItem>
          );
        })}
      </Select>
      <Select
        size="small"
        className="input"
        placeholder="points"
        value={points || "points"}
        onChange={(e) => setPoints(Number(e.target.value))}
      >
        <MenuItem value={"points"} disabled>
          Points
        </MenuItem>
        <MenuItem value={0}>0</MenuItem>
        <MenuItem value={0.25}>0.25</MenuItem>
        <MenuItem value={0.5}>0.5</MenuItem>
        <MenuItem value={0.75}>0.75</MenuItem>
        <MenuItem value={1}>1</MenuItem>
        <MenuItem value={1.25}>.125</MenuItem>
        <MenuItem value={1.5}>1.5</MenuItem>
        <MenuItem value={1.75}>1.75</MenuItem>
        <MenuItem value={2}>2</MenuItem>
      </Select>
      <TextField
        size="small"
        placeholder="assignee"
        value={assignee}
        className="input"
        onChange={(e) => setAssignee(e.target.value)}
      />
      <Button onClick={handleClick}>Add card</Button>
      {validationChek && <Alert color="error">{validationChek}</Alert>}
    </Grid>
  );
};
