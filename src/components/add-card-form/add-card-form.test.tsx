import React from "react";
import { render, cleanup } from "@testing-library/react";
import { store } from "../../redux/store";
import { Provider } from "react-redux";
import { AddCardForm } from ".";
// enzyme

afterEach(cleanup);

describe("test AddCardForm", () => {
  it("test1", () => {
    const { asFragment } = render(
      <Provider store={store}>
        <AddCardForm defaultLabel="to-do" handleClose={() => {}}/>
      </Provider>
    );
    expect(asFragment()).toMatchSnapshot();
  });
});
