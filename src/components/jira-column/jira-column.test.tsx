import React from "react";
import { render, cleanup } from "@testing-library/react";
import { JiraColumn } from ".";
import { store } from "../../redux/store";
import { Provider } from "react-redux";
import { ModalProps } from "@mui/material";
import { Card } from "../../redux/redux-slices/card-slice";

jest.mock("@mui/material", () => {
  const originalModule = jest.requireActual("@mui/material");
  return {
    __esModule: true,
    ...originalModule,
    Modal: ({ children }: ModalProps) => children,
  };
});

afterEach(cleanup);

describe("test JiraColumn", () => {
  it("test2", () => {
    const cards: Card[] = [
      {
        id: 300,
        title: "title",
        content: "content",
        label: "to-do",
        points: 0.25,
        assignee: "test",
      },
    ];
    const { asFragment } = render(
      <Provider store={store}>
        <JiraColumn label={"to-do"} cards={cards} title={"title"} />
      </Provider>
    );
    expect(asFragment()).toMatchSnapshot();
  });
});
