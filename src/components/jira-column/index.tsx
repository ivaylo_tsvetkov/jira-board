import { Grid, Box, Typography, Button, Modal } from "@mui/material";
import React, { useState } from "react";
import { AddCardForm, JiraCard } from "../../components";
import { Card, updatedCard } from "../../redux/redux-slices/card-slice";
import { useDispatch } from "react-redux";

export const JiraColumn = ({
  title,
  label,
  cards,
}: {
  title: string;
  label: string;
  cards: Card[];
}) => {
  const [addCardFormIsOpen, setAddCardFormIsOpen] = useState(false);
  const dispach = useDispatch();
  const handleClick = () => {
    setAddCardFormIsOpen(true);
  };
  const handleCloseModal = () => setAddCardFormIsOpen(false);

  return (
    <Grid item>
      <Box
        sx={{
          minHeight: "100vh",
          background: "whiteSmoke",
          width: "30vh",
          textAlign: "center",
        }}
        onDrop={(event) => {
          event.preventDefault();
          const id = Number(event.dataTransfer.getData("id"));
          dispach(updatedCard({ id, label }));
        }}
        onDragOver={(event) => event.preventDefault()}
      >
        <Typography component="h3">{title}</Typography>
        <Button onClick={handleClick}>Add card</Button>
        <Modal open={addCardFormIsOpen} onClose={handleCloseModal}>
          <>
            <AddCardForm
              defaultLabel={label}
              handleClose={() => setAddCardFormIsOpen(false)}
            />
          </>
        </Modal>
        {cards.map((card) => {
          return (
            <JiraCard
              key={card.id}
              id={card.id}
              label={card.label}
              points={card.points}
              assignee={card.assignee}
              title={card.title}
              content={card.content}
            />
          );
        })}
      </Box>
    </Grid>
  );
};
