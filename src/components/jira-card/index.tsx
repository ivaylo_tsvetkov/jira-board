import React from "react";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  Grid,
  Typography,
} from "@mui/material";
import { Card as CardProps } from "../../redux/redux-slices/card-slice";
import { useDispatch } from "react-redux";
import { deleteCard } from "../../redux/redux-slices/card-slice";

const style = {
  background: "#fceae5",
  cursor: "grabbing",
  color: "black",
  h3: {
    fontWeight: 600,
  },
  ".cardFooter": {
    alignItems: "center",
  },
};

export const JiraCard = ({
  id,
  label,
  points,
  assignee,
  title,
  content,
}: CardProps) => {
  const dispach = useDispatch();

  const handleDel = () => {
    dispach(deleteCard(id));
  };
  return (
    <Card
      className="test"
      sx={style}
      onDragStart={(event) => {
        event.dataTransfer.setData("id", id.toString());
      }}
      draggable
      onDragOver={(event) => event.preventDefault()}
    >
      <CardContent>
        <Typography component="h3">{title}</Typography>
        <Typography component="p">{content}</Typography>
      </CardContent>
      <CardActions>
        <Grid container className="cardFooter">
          <Grid item xs={8}>
            <Button onClick={handleDel}>Del</Button>
          </Grid>
          <Grid item xs={2}>
            <Typography component="span">{points}</Typography>
          </Grid>
          <Grid item xs={2}>
            <Typography component="span">{assignee}</Typography>
          </Grid>
        </Grid>
      </CardActions>
    </Card>
  );
};
