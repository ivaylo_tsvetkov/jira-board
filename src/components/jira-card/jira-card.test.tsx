import React from "react";
import { render, cleanup } from "@testing-library/react";
import { JiraCard } from ".";
import { store } from "../../redux/store";
import { Provider } from "react-redux";

afterEach(cleanup);

const a = {cards:[]}

describe("test jiraCard", () => {
  it("test1", () => {
    const { asFragment } = render(
      <Provider store={store}>
        <JiraCard
          id={300}
          title="title"
          content="content"
          label="to-do"
          points={0.25}
          assignee="test"
        />
      </Provider>
    );
    expect(asFragment()).toMatchSnapshot();
  });
});
