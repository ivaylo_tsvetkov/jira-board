const data = {
    toDo: {
        text: "TO DO",
        value: "to-do"
    },
    inProgress: {
        text: "IN PROGRESS",
        value: "in-progress"
    },
    readyForReview: {
        text: "READY FOR REVIEW",
        value: "ready-for-review"
    },
    inReview: {
        text: "IN REVIEW",
        value: "in-review"
    },
    readyToMerge: {
        text: "READY TO MERGE",
        value: "ready-to-merge"
    },
    readyToQa: {
        text: "READY TO QA",
        value: "ready-to-qa"
    },
    done: {
        text: "DONE",
        value: "done"
    },
}

export default data