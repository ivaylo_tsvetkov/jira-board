import { loadState, saveState } from "./localstorage";
import { RootState } from "./store";

const testData = {
  cards: {
    cards: [
      {
        id: 300,
        title: "title",
        content: "content",
        label: "to-do",
        points: 0.25,
        assignee: "test",
      },
      {
        id: 32,
        title: "title2",
        content: "content2",
        label: "to-do2",
        points: 0.252,
        assignee: "test2",
      },
    ],
  },
};
let store: any = {};
const localStorage = {
  getItem: function (key: string) {
    return store[key] || null;
  },
  setItem: function (key: string, value: RootState) {
    store[key] = value.toString();
  },
};

Object.defineProperty(global, "localStorage", {
  value: localStorage,
});

test("setState", () => {
  expect(saveState(testData)).toEqual(store.state);
});
test("loadState", () => {
  expect(loadState()).toEqual(testData.cards);
});
