import { RootState } from "../../../store";
import { createSelector } from "reselect";

export const cardsSelector = createSelector(
  (state: RootState) => state.cards.cards,
  (cards) => cards
);
