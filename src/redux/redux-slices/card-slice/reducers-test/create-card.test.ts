import reducer, { createdCard, CardsState } from "../index"

test('should return the initial state', () => {
    expect(reducer(undefined, { type: "card" })).toEqual({ cards: [] })
})


test('should handle a card being added to an empty list', () => {
    const previousState: CardsState = { cards: [] };
    expect(reducer(previousState, createdCard({
        id: 300,
        title: "title",
        content: "content",
        label: "to-do",
        points: 0.25,
        assignee: "test"
    }))).toEqual({
        cards: [{
            id: 300,
            title: "title",
            content: "content",
            label: "to-do",
            points: 0.25,
            assignee: "test"
        }]
    })
})

test('should handle a card being added to an existing list', () => {
    const previousState: CardsState = {
        cards: [{
            id: 300,
            title: "title",
            content: "content",
            label: "to-do",
            points: 0.25,
            assignee: "test"
        }]
    };
    expect(reducer(previousState, createdCard({
        id: 30,
        title: "title2",
        content: "content2",
        label: "to-do2",
        points: 0.5,
        assignee: "test2"
    }))).toEqual({
        cards: [{
            id: 300,
            title: "title",
            content: "content",
            label: "to-do",
            points: 0.25,
            assignee: "test"
        }, {
            id: 30,
            title: "title2",
            content: "content2",
            label: "to-do2",
            points: 0.5,
            assignee: "test2"
        }]
    })
})
