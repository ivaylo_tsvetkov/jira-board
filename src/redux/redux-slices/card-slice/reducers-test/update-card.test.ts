import reducer, { updatedCard, CardsState } from "../index"

test('should handle a card being updated from an existing list', () => {
    const previousState: CardsState = {
        cards: [{
            id: 30,
            title: "title",
            content: "content",
            label: "to-do",
            points: 0.25,
            assignee: "test"
        }]
    };
    expect(reducer(previousState, updatedCard({
        id: 30,
        label: "done",
    }))).toEqual({
        cards: [{
            id: 30,
            title: "title",
            content: "content",
            label: "done",
            points: 0.25,
            assignee: "test"
        }]
    })
})