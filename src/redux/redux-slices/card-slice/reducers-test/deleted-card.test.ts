import reducer, { CardsState, deleteCard } from "../index"

test('should handle a card being deleted from an existing list', () => {
    const previousState: CardsState = {
        cards: [{
            id: 30,
            title: "title",
            content: "content",
            label: "to-do",
            points: 0.25,
            assignee: "test"
        },
        {
            id: 300,
            title: "title",
            content: "content",
            label: "to-do",
            points: 0.25,
            assignee: "test"
        },]
    };
    expect(reducer(previousState, deleteCard(30))).toEqual({
        cards: [{
            id: 300,
            title: "title",
            content: "content",
            label: "to-do",
            points: 0.25,
            assignee: "test"
        }]
    })
})