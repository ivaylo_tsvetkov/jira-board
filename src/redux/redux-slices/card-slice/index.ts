import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { loadState } from "../../localstorage";

export interface Card {
  id: number;
  label: string;
  points: number;
  title: string;
  content: string;
  assignee: string;
}

export interface CardsState {
  cards: Card[];
}

const initialState: CardsState = {
  cards: loadState()?.cards || [],
};

export const cardsSlice = createSlice({
  name: "cards",
  initialState,
  reducers: {
    createdCard: (state, action: PayloadAction<Card>) => {
      return {
        ...state,
        cards: [...state.cards, action.payload],
      };
    },
    updatedCard: (
      state,
      action: PayloadAction<{ id: number; label: string }>
    ) => {
      const newCard = state.cards.find((x) => x.id === action.payload.id);
      if (!newCard) {
        return state;
      }
      const newState = [
        ...state.cards.filter((e) => e.id !== action.payload.id),
        { ...newCard, label: action.payload.label },
      ];
      return {
        ...state,
        cards: newState,
      };
    },
    deleteCard: (state, action: PayloadAction<number>) => {
      return {
        ...state,
        cards: state.cards.filter((e) => e.id !== action.payload),
      };
    },
  },
});

// Action creators are generated for each case reducer function
export const { createdCard, updatedCard, deleteCard } = cardsSlice.actions;

export default cardsSlice.reducer;
