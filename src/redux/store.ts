import { configureStore } from "@reduxjs/toolkit";
import cardsReducer from "./redux-slices/card-slice";
import { saveState } from "./localstorage";

export const store = configureStore({
  reducer: {
    cards: cardsReducer,
  },
});

store.subscribe(() => {
  saveState(store.getState());
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
