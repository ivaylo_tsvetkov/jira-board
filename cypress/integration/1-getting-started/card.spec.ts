/// <reference types="cypress" />

describe("test-card", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/");
    cy.contains("Add card").click();
    cy.get(".input").each((e, i) => {
      if (i === 0 || i === 1 || i === 4) {
        cy.wrap(e).type("to-do");
      } else if (i === 2) {
        cy.wrap(e).click();
        cy.get(".MuiMenuItem-root").first().click();
      } else {
        cy.wrap(e).click();
        cy.get(".MuiMenuItem-root").eq(2).click();
      }
    });
    cy.get(".MuiModal-root button").click();
  });
  it("Create а card at click button Add-card in add-card-form", () => {
    cy.get("#root > div > div > div .MuiCard-root").should("have.length", 1);
  });
  it("Card is in the appropriate column", () => {
    cy.get("#root > div > div > div").first().get(".MuiCard-root");
    cy.window().its('store').invoke('getState').its("cards").its("cards").should("deep.equal", [{
      id: 0,
      title:"to-do",
      content:"to-do",
      label:"to-do",
      points: 0.25,
      assignee:"to-do",
    }])
  });
  it("Тhe card can be drag and drop to another column", () => {
    const dataTransfer = new DataTransfer();
    cy.get(".MuiCard-root").first().trigger("dragstart", { dataTransfer });
    cy.get("#root > div > div > div").eq(3).trigger("drop", {
      dataTransfer,
    });
    cy.reload();
    cy.window().its('store').invoke('getState').its("cards").its("cards").should("deep.equal", [{
      id: 0,
      title:"to-do",
      content:"to-do",
      label:"in-review",
      points: 0.25,
      assignee:"to-do",
    }])
  });
  it("Тhe card can be delete", () => {
    cy.get(".MuiCard-root").contains("Del").first().click();
      cy.get(".MuiCard-root").should("have.length", 0)
      cy.window().its('store').invoke('getState').its("cards").its("cards").should("have.length", 0)
  });
});
export {};
