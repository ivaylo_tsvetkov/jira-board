/// <reference types="cypress" />

describe("e2e Tests", () => {
  beforeEach(() => {
    // Cypress starts out with a blank slate for each test
    // so we must tell it to visit our website with the `cy.visit()` command.
    // Since we want to visit the same URL at the start of all our tests,
    // we include it in our beforeEach function so that it runs before each test
    cy.visit("http://localhost:3000/");
  });

  it("displays seven columns", () => {
    // We use the `cy.get()` command to get all elements that match the selector.
    // Then, we use `should` to assert that there are seven matched items.
    cy.get("#root > div > div > div").should("have.length", 7);
  });
  it("displays addCard button", () => {
    cy.contains("Add card");
  });
  it("displays addCard form", () => {
    cy.contains("Add card").click();
    cy.get("input").should("have.length", 4);
    cy.get(".MuiModal-root button");
  });
  it("addCard form have button Add card", () => {
    cy.contains("Add card").click();
    cy.get(".MuiModal-root button").type("Add card");
  });
});

export {};
